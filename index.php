<?php
include("db_inc.php");
?>
<html>
  <head>
    <script src="js/amcharts/amcharts.js" type="text/javascript"></script>
  	<script src="js/amcharts/plugins/responsive/responsive.min.js" type="text/javascript"></script>
  	<script src="js/amcharts/themes/light.js" type="text/javascript"></script>
  	<link rel="stylesheet" href="js/ammap/ammap.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/main.css">
  	<script src="js/ammap/ammap.js" type="text/javascript"></script>
  	<script src="js/ammap/maps/js/indiaLow.js" type="text/javascript"></script>
  	<script src="js/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">


        var totalStudents;
        var totalInstitutes;
        var totalCities;
        var allData = [];
        var organisedData = [];

        $.ajax({
              type : "POST",
              cache: false,
              url : "get_details.php",
              success: function(data){
                allData = data;
                stateData =  makeSense(data);
                var mapData = {map : "indiaLow", areas : stateData};

                // console.log(mapData);
                drawMap(mapData);
              },
              error: function(error){
                console.log(error);
              }
        });

        function filter(data, term) {

        }

        function makeSense(data) {
          var stateArray = [];
          var states = [];
          var cities = {};
          for (i in data) {
            if(states.indexOf(data[i].state) == -1) {
              states.push(data[i].state);
              cities[data[i].state] = {};
              cities[data[i].state].cityList = [];
            }
            if(cities[data[i].state]['cityList'].indexOf(data[i].cluster) == -1) {
              cities[data[i].state]['cityList'].push(data[i].cluster);
            }
          }

          // console.log(cities);
          for (state in states) {
            stateArray.push({"id" : states[state], "value" : 0});
            // organisedData.push({"state":states[state], ""});
          }

          for (i in data) {
            for (state in stateArray) {
              if(data[i].state == stateArray[state].id) {
                stateArray[state].value += 1;
                break;
              }
            }
          }

          return stateArray;
        }

    		function drawMap(data) {
    			var map = AmCharts.makeChart( "chartdiv", {
    				type: "map",
    				"theme": "light",

    				colorSteps: 30,

    				dataProvider: data,

    				areasSettings: {
    					autoZoom: false,
              selectedColor: "ff6666",
              selectable : true,
              // rollOverColor : "ff6666"
    					// color : "044704",
    					// colorSolid : "b2600c"
    				},

    				valueLegend: {
    				 right: 10,
    				 minValue: "Fewer Students",
    				 maxValue: "More Students"
     				},

    				zoomControl: {
    					zoomControlEnabled:false,
    					panControlEnabled:false
    				},



    				"export": {
    					"enabled": false
    				}

    		} );

        map.balloonLabelFunction = getBalloonText;

        function getBalloonText(mapObject, ammap) {

          return mapObject.title;
        }

    		map.addListener("clickMapObject", function (event) {

          // deselect the area by assigning all of the dataProvider as selected object
          map.selectedObject = map.dataProvider;

          // toggle showAsSelected
          event.mapObject.showAsSelected = !event.mapObject.showAsSelected;

          // bring it to an appropriate color
          map.returnInitialColor( event.mapObject );

          // let's build a list of currently selected states
          var selectedStates = [];
          for ( var i in map.dataProvider.areas ) {
            var area = map.dataProvider.areas[ i ];
            if ( area.showAsSelected ) {
              selectedStates.push( area.id);
            }
          }

          // console.log(selectedStates);

    			// var filteredStudents = [];
          // var filteredInstitutes = [];
          // var filteredCities = [];
    			// for (i in allData) {
    			// 	if(allData[i].state == event.mapObject.id) {
    			// 		filteredStudents.push(allData[i]);
          //     if(allData[i].institute) {
          //
          //     }
          //     if(allData[i].city) {
          //
          //     }
    			// 	}
    			// }
    			// var slider = $('#slider').slideReveal("show");

          var stateData = [];
          var cities = [];
          var institutes = [];
          for (state in selectedStates) {
            var tmpData = allData.filter(function(e) {
              return e.state == selectedStates[state];
            });
            stateData = stateData.concat(tmpData);
          }

          for (i in stateData) {
              if(cities.indexOf(stateData[i].cluster) == -1) {
                cities.push(stateData[i].cluster);
              }

              if(institutes.indexOf(stateData[i].institute) == -1) {
                institutes.push(stateData[i].institute);
              }
          }

          console.log(stateData);
          console.log(cities);
          console.log(institutes);
    		});

    }

    function filter(key, value, data) {
      data = data.filter(function(e) {
        return e[key] = value;
      });

      return data;
    }

    function search(term, data) {
      data = data.filter(function(e) {
        return (e.name.indexOf(term) != -1)
            || (e.institute.indexOf(term) != -1)
            || (e.phone.indexOf(term) != -1)
            || (e.city.indexOf(term) != -1)
            || (e.cluster.indexOf(term) != -1)
            || (e.email.indexOf(term) != -1)
            || (e.last_name.indexOf(term) != -1) ;
      });

      return data;
    }

    </script>

    <style>
  	#chartdiv {
  		margin: auto;
  	 	width: 50%;
      padding-top: 10px;
  	}

  	</style>

  </head>
  <body>
    <div class="navbar navbar-default" role="navigation">
      <div class="menu-items">
        <div class="navbar-header">
          <a class="navbar-brand" href="">
          <span class="navbar-title">Student Map for NEN</span>
          </a>
        </div>
      </div>
    </div>
    </div>
    <div id="chartdiv" style="width: 800px; height: 600px;"></div>
  </body>
</html>
