<?php

header('Content-Type: application/json');

include("db_inc.php");
include("dump.php");

$results_array = array();
$select = mysqli_query($con, "SELECT * FROM students");
while($return = mysqli_fetch_assoc($select)) {
  $results_array[] = array("email" => $return["email"], "phone" => $return["phone"], "name" => $return["name"], "last_name" => $return["last_name"], "institute" => $return["institute"], "city" => $return["city"], "cluster" => $return["cluster"], "state" => $return["state"]);
}

echo json_encode($results_array);
// do_dump($results_array);



?>
